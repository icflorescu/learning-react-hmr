import React from 'react';

import Title from './Title';

export default props => (
  <div class="app">
    <Title/>
    <p>Is this working?</p>
    <p>Kind of... I think.</p>
    <p>Yep, it's working perfectly!... Or is it?</p>
    <p>In fact it is.</p>
    <p>It's keeping the state allright, it just repaints the wholelotta' page when <strong>style<em>shites</em></strong> are changed.</p>
    <p>Which is actually normal. Kind of. Or not. But not an issue anyway.</p>
    <p>But it does keep the state.</p>
  </div>
);
