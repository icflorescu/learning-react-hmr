import React from 'react';

export default class Title extends React.Component {
  state = { txt: 'wut?!?' };

  onTitleClick = (e) => {
    this.setState({ txt: 'reloading' });
  };

  render() {
    return <h2 onClick={this.onTitleClick}>Hot {this.state.txt}...</h2>;
  }
};

// export default props => (
//   <h2>Hot reloading?!?...</h2>
// );
