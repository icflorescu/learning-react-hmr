const { resolve }       = require('path'),
      webpack           = require('webpack'),
      autoprefixer      = require('autoprefixer-stylus'),
      ExtractTextPlugin = require('extract-text-webpack-plugin'),
      HtmlWebpackPlugin = require('html-webpack-plugin');

const entry = ['./index.js'],
      plugins = [
        new HtmlWebpackPlugin({
          template: 'index.html', minify: { html5: true, collapseWhitespace: true }
        }),
        new webpack.LoaderOptionsPlugin({ options: {
          stylus: { use: [autoprefixer({ browsers: ['> 5%'] })] },
          context: '/'
        } }),
      ];

module.exports = env => {
  const dev = env ? env.dev : false;
  return {
    output: { path: resolve(__dirname, 'dist'), filename: 'index.min.js', publicPath: '/' },
    context: resolve(__dirname, 'src'),
    devtool: dev ? 'inline-source-map' : 'hidden-source-map',
    devServer: { hot: true, contentBase: '/dist', publicPath: '/' },

    entry: dev ? [
      'react-hot-loader/patch',
      'webpack-dev-server/client?http://localhost:8080',
      'webpack/hot/only-dev-server',
    ].concat(entry) : entry,

    module: {
      loaders: [{
        test: /\.js$/,
        loaders: ['babel'], exclude: /node_modules/
      }, {
        test: /\.styl$/,
        loaders: dev
          ? 'style!css?sourceMap!stylus?sourceMap'
          : ExtractTextPlugin.extract('css?minimize&sourceMap!stylus?sourceMap')
      }],
    },

    plugins: dev ? plugins.concat([
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
    ]) : plugins.concat([
      new webpack.optimize.DedupePlugin(),
      new webpack.DefinePlugin({ 'process.env': { NODE_ENV: JSON.stringify('production') } }),
      new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false }, output: { comments: false } }),
      new ExtractTextPlugin('index.min.css'),
    ])
  };
};
